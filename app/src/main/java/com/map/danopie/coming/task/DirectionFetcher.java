package com.map.danopie.coming.task;

import android.os.AsyncTask;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by Danopie on 5/15/2017.
 */

public class DirectionFetcher extends AsyncTask<Void, Integer, ArrayList<LatLng>> {

    public interface OnDirectionFoundListener {
        void onDirectionFound(ArrayList<LatLng> result);
    }

    private static final String googleDirectionURL = "http://maps.googleapis.com/maps/api/directions/json";
    private static final String TAG = DirectionFetcher.class.getSimpleName();
    private OnDirectionFoundListener listener;

    private LatLng fromLocation;
    private LatLng toLocation;
    private URL url;

    public DirectionFetcher setOnDirectionFoundListener(OnDirectionFoundListener listener){
        this.listener = listener;
        return this;
    }

    public DirectionFetcher setFromLocation(LatLng fromLocation){
        this.fromLocation = fromLocation;
        return this;
    }

    public DirectionFetcher setToLocation(LatLng toLocation){
        this.toLocation = toLocation;
        return this;
    }

    private String buildURL(LatLng from, LatLng to){
        StringBuilder urlString = new StringBuilder();
        urlString.append(googleDirectionURL);
        urlString.append("?origin=");
        urlString.append(Double.toString(from.latitude));
        urlString.append(",");
        urlString.append(Double.toString(from.longitude));
        urlString.append("&destination=");
        urlString.append(Double.toString(to.latitude));
        urlString.append(",");
        urlString.append(Double.toString(to.longitude));
        urlString.append("&sensor=false");
        return urlString.toString();
    }

    @Override
    protected ArrayList<LatLng> doInBackground(Void... params) {
        try {
            url = new URL(buildURL(fromLocation,toLocation));
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setDoInput(true);
            con.setRequestProperty("Content-Type","application/json");
            con.setRequestProperty("Accept", "application/json; charset=UTF-8");
            con.setRequestMethod("GET");

            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer stringResponse = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                stringResponse.append(inputLine);
            }
            in.close();
            JSONObject jsonResponse = new JSONObject(stringResponse.toString());
            JSONArray routes = jsonResponse.getJSONArray("routes");
            JSONObject route = routes.getJSONObject(0);
            JSONObject overviewPolylines = route.getJSONObject("overview_polyline");
            String points = overviewPolylines.getString("points");
            return decodePolylines(points);

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPostExecute(ArrayList<LatLng> latLngs) {
        super.onPostExecute(latLngs);
        if(listener != null){
            listener.onDirectionFound(latLngs);
        }
    }

    private ArrayList<LatLng> decodePolylines(String encoded) {
        ArrayList<LatLng> poly = new ArrayList<LatLng>();
        int index = 0, len = encoded.length();
        int lat = 0, lng = 0;

        while (index < len) {
            int b, shift = 0, result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;

            shift = 0;
            result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            LatLng p = new LatLng((((double) lat / 1E5)),(((double) lng / 1E5)));
            poly.add(p);
        }
        return poly;
    }



}

