package com.map.danopie.coming.task;

import android.os.AsyncTask;

import com.map.danopie.coming.model.User;
import com.map.danopie.coming.service.ComingServices;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by Danopie on 5/18/2017.
 */

public class FriendListFetcher extends AsyncTask<Void, Integer, ArrayList<User>> {
    private static final String TAG = FriendListFetcher.class.getSimpleName();
    private static final String MALE = "1";
    private Integer userId;
    private OnFriendFoundListener listener;
    ArrayList<User> friends = new ArrayList<User>();

    public interface OnFriendFoundListener {
        void onFriendFound(ArrayList<User> friends);
    }

    public FriendListFetcher setUserId(Integer userId) {
        this.userId = userId;
        return this;
    }

    public FriendListFetcher setOnFriendFoundListener(OnFriendFoundListener listener) {
        this.listener = listener;
        return this;
    }

    @Override
    protected ArrayList<User> doInBackground(Void... params) throws NullPointerException {
        if (userId == null) {
            throw new NullPointerException("Missing Resources");
        }
        try {
            URL url = new URL(ComingServices.comingUrl + "friend/" + "?id=" + userId);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setDoInput(true);
            con.setRequestProperty("Content-Type", "application/json");
            con.setRequestProperty("Accept", "application/json; charset=UTF-8");
            con.setRequestMethod("GET");

            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer stringResponse = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                stringResponse.append(inputLine);
            }
            in.close();

            JSONObject jsonResponse = new JSONObject(stringResponse.toString());
            JSONArray datas = jsonResponse.getJSONArray("data");
            for (int i = 0; i < datas.length(); i++) {
                JSONObject data = datas.getJSONObject(i);
                Integer id = Integer.parseInt(data.getString("id"));
                String name = data.getString("name");
                String phone = data.getString("phone");
                Boolean gender = getGender(data.getString("gender"));
                friends.add(new User(id, name, phone, gender));
            }
            return friends;

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return friends;
    }

    @Override
    protected void onPostExecute(ArrayList<User> friends) {
        super.onPostExecute(friends);
        if (listener != null) {
            listener.onFriendFound(friends);
        }
    }

    private Boolean getGender(String gender){
        return gender.equals(MALE);
    }
}
