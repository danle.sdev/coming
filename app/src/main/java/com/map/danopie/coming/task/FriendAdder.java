package com.map.danopie.coming.task;

import android.os.AsyncTask;
import android.util.Log;

import com.map.danopie.coming.service.ComingServices;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

/**
 * Created by Danopie on 5/21/2017.
 */

public class FriendAdder extends AsyncTask<Void, Integer, Boolean> {
    public interface OnFriendAddedListener{
        void onFriendAdded(Boolean added);
    }
    private static final String TAG = FriendAdder.class.getSimpleName();
    private Integer userId;
    private Integer friendId;
    private OnFriendAddedListener listener;

    public FriendAdder setUserId(Integer userId) {
        this.userId = userId;
        return this;
    }

    public FriendAdder setFriendId(Integer friendId) {
        this.friendId = friendId;
        return this;
    }

    public FriendAdder setOnFriendAddedListener(OnFriendAddedListener listener) {
        this.listener = listener;
        return this;
    }

    @Override
    protected Boolean doInBackground(Void... params) {
        if(userId == null || friendId == null){
            throw new NullPointerException("Missing Resources");
        }
        try {
            URL url = new URL(ComingServices.comingUrl + "friend/" + "?id=" + userId);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setDoInput(true);
            con.setDoOutput(true);
            con.setRequestProperty("Content-Type", "application/json");
            con.setRequestProperty("Accept", "application/json; charset=UTF-8");
            con.setRequestMethod("POST");
            JSONObject req = new JSONObject();
            req.put("friend_id",friendId);
            OutputStreamWriter wr = new OutputStreamWriter(con.getOutputStream());
            wr.write(req.toString());
            wr.flush();
            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer stringResponse = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                stringResponse.append(inputLine);
            }
            in.close();
            Log.d(TAG, stringResponse.toString());
            JSONObject res = new JSONObject(stringResponse.toString());
            String val = res.getString("status");
            Boolean status = Boolean.parseBoolean(val);
            return isAdded(status);


        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

    private Boolean isAdded(Boolean status) {
        return !status;
    }

    @Override
    protected void onPostExecute(Boolean added) {
        if(listener!= null){
            listener.onFriendAdded(added);
        }
    }

}
