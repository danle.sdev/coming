package com.map.danopie.coming.task;

import android.os.AsyncTask;
import android.util.Log;

import com.map.danopie.coming.model.User;
import com.map.danopie.coming.service.ComingServices;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

/**
 * Created by Danopie on 5/20/2017.
 */

public class UserFetcher extends AsyncTask<Void,Integer,User>  {
    private static final String TAG = UserFetcher.class.getSimpleName();
    private String phoneNumber;
    private Integer id;
    private OnUserFetchedListener listener;
    public interface OnUserFetchedListener {
        void onUserFetched(User user);
    }

    public UserFetcher setPhoneNumber(String phoneNumber) {
        id = null;
        this.phoneNumber = phoneNumber;
        return this;
    }

    public UserFetcher setId(Integer id){
        phoneNumber = null;
        this.id = id;
        return this;
    }

    public UserFetcher setOnUserFetchedListener(OnUserFetchedListener listener) {
        this.listener = listener;
        return this;
    }

    @Override
    protected User doInBackground(Void... params) throws NullPointerException {
        if(phoneNumber == null && id == null){
            throw new NullPointerException("Missing Resources");
        }
        try {
            URL url;
            if(phoneNumber != null){
                url = new URL(ComingServices.comingUrl + "user/" + "?phone=" + phoneNumber);
            } else {
                url = new URL(ComingServices.comingUrl + "user/" + "?id=" + id);
            }
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setDoInput(true);
            con.setRequestProperty("Content-Type", "application/json");
            con.setRequestProperty("Accept", "application/json; charset=UTF-8");
            con.setRequestMethod("GET");
            Integer responseCode = con.getResponseCode();
            Log.d(TAG, responseCode.toString());
            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer stringResponse = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                stringResponse.append(inputLine);
            }
            in.close();
            Log.d(TAG, stringResponse.toString());
            JSONObject jsonResponse = new JSONObject(stringResponse.toString());
            JSONObject data = jsonResponse.getJSONObject("data");
            Integer id = Integer.parseInt(data.getString("id"));
            String name = data.getString("name");
            String phone = data.getString("phone");
            Boolean gender = getGender(data.getString("gender"));
            User user = new User(id,name,phone,gender);
            return user;

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;

    }

    @Override
    protected void onPostExecute(User user) {
        if(listener!= null){
            listener.onUserFetched(user);
        }
    }

    private Boolean getGender(String gender){
        return gender.equals("1");
    }
}
