package com.map.danopie.coming.task;

import android.os.AsyncTask;

import com.google.android.gms.maps.model.LatLng;
import com.map.danopie.coming.service.ComingServices;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

/**
 * Created by Danopie on 5/18/2017.
 */

public class LocationFetcher extends AsyncTask<Void,Integer,LatLng> {
    private static final String TAG = LocationFetcher.class.getSimpleName();
    public interface OnLocationFoundListener{
        void onLocationFound(LatLng location);
    }

    private Integer userId;
    private OnLocationFoundListener listener;

    public LocationFetcher setId(Integer id){
        this.userId = id;
        return this;
    }

    public LocationFetcher setOnLocationFoundListener(OnLocationFoundListener listener){
        this.listener = listener;
        return this;
    }

    @Override
    protected LatLng doInBackground(Void... params) throws NullPointerException {
        if(userId == null){
            throw new NullPointerException("Missing resouces");
        }
        try {
            URL url = new URL(ComingServices.comingUrl + "location/" + "?id="+ userId);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setDoInput(true);
            con.setRequestProperty("Content-Type", "application/json");
            con.setRequestProperty("Accept", "application/json; charset=UTF-8");
            con.setRequestMethod("GET");

            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer stringResponse = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                stringResponse.append(inputLine);
            }
            in.close();
            //Log.d(TAG, stringResponse.toString());
            JSONObject jsonResponse = new JSONObject(stringResponse.toString());
            JSONObject data = jsonResponse.getJSONObject("data");

            return new LatLng(Float.parseFloat(data.getString("latitude"))
                    ,Float.parseFloat(data.getString("longitude")));

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
           e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(LatLng latLng) {
        super.onPostExecute(latLng);
        if(listener != null){
            listener.onLocationFound(latLng);
        }
    }
}
