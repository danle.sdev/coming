package com.map.danopie.coming.task;

import android.os.AsyncTask;

import com.google.android.gms.maps.model.LatLng;
import com.map.danopie.coming.service.ComingServices;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

/**
 * Created by Danopie on 5/15/2017.
 */

public class LocationUpdater extends AsyncTask<Void, Integer, Void>{
    private final static String TAG = LocationUpdater.class.getSimpleName();
    private LatLng location;
    private Integer userId;

    @Override
    protected Void doInBackground(Void... params) throws NullPointerException {
        if(location == null || userId == null) {
            throw new NullPointerException("Missing resource");
        }
        try {
            URL url = new URL(ComingServices.comingUrl + "location/" + "?id="+ userId);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setDoOutput(true);
            con.setDoInput(true);
            con.setRequestProperty("Content-Type", "application/json");
            con.setRequestProperty("Accept", "application/json; charset=UTF-8");
            con.setRequestMethod("POST");
            JSONObject req = new JSONObject();
            req.put("longitude",location.longitude);
            req.put("latitude",location.latitude);
            OutputStreamWriter wr = new OutputStreamWriter(con.getOutputStream());
            wr.write(req.toString());
            wr.flush();

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }
    public LocationUpdater setLocation(LatLng location){
        this.location = location;
        return this;
    }
    public LocationUpdater setId(int userId){
        this.userId = userId;
        return this;
    }
}
