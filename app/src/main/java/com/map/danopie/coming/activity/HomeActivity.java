package com.map.danopie.coming.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.location.places.ui.SupportPlaceAutocompleteFragment;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.map.danopie.coming.fragment.AddFriendFragment;
import com.map.danopie.coming.fragment.DirectionFragment;
import com.map.danopie.coming.fragment.FabFragment;
import com.map.danopie.coming.FriendAdapter;
import com.map.danopie.coming.R;
import com.map.danopie.coming.fragment.SearchFragment;
import com.map.danopie.coming.fragment.SearchFriendFragment;
import com.map.danopie.coming.model.User;
import com.map.danopie.coming.service.ComingServices;
import com.map.danopie.coming.service.GoogleMapServices;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import java.util.ArrayList;

import static java.security.AccessController.getContext;

public class HomeActivity extends AppCompatActivity {
    enum FriendPanelState {
        ADD,
        SEARCH,
        NONE
    }

    enum MapState{
        SEARCH,
        DIRECTION
    }

    private final static String TAG = HomeActivity.class.getSimpleName();
    private final static int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;
    private final static float NORMAL_TEXT_SIZE = 16.0f;
    private final static String FONT_PATH = "font/Lato-Regular.ttf";
    private FriendPanelState friendPanelState;
    private MapState mapState;

    private GoogleMapServices googleMapServices;
    private ComingServices comingServices;

    private SupportMapFragment mapFragment;
    private FabFragment fabFragment;
    private DirectionFragment directionFragment;
    private SearchFragment searchFragment;
    private AddFriendFragment addFriendFragment;
    private SearchFriendFragment searchFriendFragment;
    private SupportPlaceAutocompleteFragment autocompleteDirectionFragment;
    private SupportPlaceAutocompleteFragment autocompleteSearchFragment;
    private SlidingUpPanelLayout slidingUpPanelLayout;
    private FriendAdapter friendAdapter;
    private RecyclerView friendPanel;

    private DrawerLayout drawerLayout;
    private NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        googleMapServices = GoogleMapServices.getServices(this);
        comingServices = ComingServices.getServices(this);


        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int id = item.getItemId();
                if(id==R.id.nav_item_logout){
                    drawerLayout.closeDrawer(GravityCompat.START);
                    onUserLogout();
                } else if(id == R.id.nav_item_map){
                    Log.d(TAG,"Main map");
                } else if(id == R.id.nav_item_option){
                    Log.d(TAG,"Option Pressed");
                }
                return true;
            }
        });

        friendPanelState = FriendPanelState.NONE;
        mapState = MapState.SEARCH;
        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(googleMapServices);
        searchFragment = (SearchFragment) getSupportFragmentManager().findFragmentById(R.id.search_fragment);

        fabFragment = new FabFragment();
        addFriendFragment = new AddFriendFragment();
        searchFriendFragment = new SearchFriendFragment();
        directionFragment = new DirectionFragment();
        slidingUpPanelLayout = (SlidingUpPanelLayout) findViewById(R.id.sliding_panel);
        friendPanel = (RecyclerView) findViewById(R.id.friend_panel);

        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.container, fabFragment)
                .add(R.id.top_panel, directionFragment)
                .hide(directionFragment)
                .add(R.id.dialog_friend_view, addFriendFragment)
                .hide(addFriendFragment)
                .add(R.id.dialog_friend_view, searchFriendFragment)
                .hide(searchFriendFragment)
                .commit();
    }

    private void onUserLogout() {
        comingServices.clearUser();
        Intent loginIntent = new Intent(this ,LoginActivity.class);
        loginIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(loginIntent);
        ActivityCompat.finishAffinity(this);
    }

    public void setUserInfo(String name, Boolean gender) {
        ImageView userPic = (ImageView) findViewById(R.id.nav_header_picture);
        int pictureId = gender?R.drawable.profile_picture_male:R.drawable.profile_picture_female;
        //userPic.setBackground(getApplicationContext().getDrawable(pictureId));
        userPic.setBackgroundResource(pictureId);


        TextView userName = (TextView) findViewById(R.id.nav_header_name);
        userName.setText(name);


    }

    @Override
    protected void onResume() {
        super.onResume();
        //Setup Search Bar
        autocompleteSearchFragment = (SupportPlaceAutocompleteFragment) searchFragment
                .getChildFragmentManager()
                .findFragmentById(R.id.place_autocomplete_fragment_search);
        autocompleteSearchFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                googleMapServices.moveViewToLocation(place.getLatLng());
                googleMapServices.setLocationFrom(place.getLatLng());
                googleMapServices.setSearchMarker(place.getLatLng());
            }

            @Override
            public void onError(Status status) {
                Log.d(TAG, status.toString());
            }
        });
        autocompleteSearchFragment.getView()
                .findViewById(R.id.place_autocomplete_search_button)
                .setVisibility(View.INVISIBLE);
        ((EditText) (autocompleteSearchFragment.getView()
                .findViewById(R.id.place_autocomplete_search_input)))
                .setTextSize(NORMAL_TEXT_SIZE);
        ((EditText) (autocompleteSearchFragment.getView()
                .findViewById(R.id.place_autocomplete_search_input)))
                .setTypeface(Typeface.createFromAsset(getApplicationContext()
                        .getAssets(), FONT_PATH));
        ((EditText) (autocompleteSearchFragment.getView()
                .findViewById(R.id.place_autocomplete_search_input)))
                .setTextColor(ContextCompat.getColor(this, R.color.colorPrimaryText));
        ((AppCompatImageButton) (autocompleteSearchFragment.getView()
                .findViewById(R.id.place_autocomplete_clear_button)))
                .setVisibility(View.INVISIBLE);

        //Setup Direction Bar
        autocompleteDirectionFragment = (SupportPlaceAutocompleteFragment) directionFragment
                .getChildFragmentManager()
                .findFragmentById(R.id.place_autocomplete_fragment_direction);
        autocompleteDirectionFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                if (mapState == MapState.DIRECTION) {
                    googleMapServices.setLocationTo(place.getLatLng());
                    googleMapServices.setDirectionMarker(place.getLatLng());
                    googleMapServices.moveViewToLocation(place.getLatLng());
                }
            }

            @Override
            public void onError(Status status) {
                Log.d(TAG, status.toString());
            }
        });
        autocompleteDirectionFragment.getView()
                .findViewById(R.id.place_autocomplete_search_button)
                .setVisibility(View.INVISIBLE);
        ((EditText) (autocompleteDirectionFragment.getView()
                .findViewById(R.id.place_autocomplete_search_input)))
                .setTextSize(NORMAL_TEXT_SIZE);
        ((EditText) (autocompleteDirectionFragment.getView()
                .findViewById(R.id.place_autocomplete_search_input)))
                .setTypeface(Typeface.createFromAsset(getApplicationContext()
                        .getAssets(), FONT_PATH));
        ((EditText) (autocompleteDirectionFragment.getView()
                .findViewById(R.id.place_autocomplete_search_input)))
                .setTextColor(ContextCompat.getColor(this, R.color.colorPrimaryText));
        ((EditText) (autocompleteDirectionFragment.getView()
                .findViewById(R.id.place_autocomplete_search_input)))
                .setHint("To");
        ((AppCompatImageButton) (autocompleteDirectionFragment.getView()
                .findViewById(R.id.place_autocomplete_clear_button)))
                .setVisibility(View.INVISIBLE);

        EditText searchFriendText = (EditText) searchFriendFragment.getView().findViewById(R.id.search_friend_text);

        searchFriendText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //Do nothing
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                friendAdapter.update(comingServices.filterFriendByName(s));
            }

            @Override
            public void afterTextChanged(Editable s) {
                //Do nothing
            }
        });
    }

    public void onNavigationButtonClicked(View view) {
        drawerLayout.openDrawer(GravityCompat.START);
    }


    public void onSearchMyLocationButtonClicked(View view) {
        LatLng lc = googleMapServices.getCurrentLocation();
        if (lc != null) {
            googleMapServices.setLocationFrom(lc);
            googleMapServices.moveViewToLocation(lc);
            googleMapServices.setSearchMarker(lc);
            ((EditText) autocompleteSearchFragment.getView()
                    .findViewById(R.id.place_autocomplete_search_input))
                    .setText("Your Location");
        } else {
            displayMessage("Cannot retrieve your current location");
        }
    }

    public void onFABClicked(View view) {
        setMapState(MapState.DIRECTION);
    }

    private void setMapState(MapState state) {
        mapState = state;
        if (mapState == MapState.DIRECTION) {
            getSupportFragmentManager().beginTransaction()
                    .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                    .show(directionFragment)
                    .hide(fabFragment)
                    .commit();
            ((EditText) (autocompleteSearchFragment.getView()
                    .findViewById(R.id.place_autocomplete_search_input)))
                    .setHint("From");
        } else if (mapState == MapState.SEARCH) {
            ((EditText) (autocompleteDirectionFragment.getView()
                    .findViewById(R.id.place_autocomplete_search_input)))
                    .setText("");
            getSupportFragmentManager().beginTransaction()
                    .setCustomAnimations(0, android.R.anim.slide_out_right)
                    .hide(directionFragment)
                    .show(fabFragment)
                    .commit();
            ((EditText) (autocompleteSearchFragment.getView()
                    .findViewById(R.id.place_autocomplete_search_input)))
                    .setHint("Search");
            googleMapServices.clearDirection();
        }
    }

    public void onDirectionBackButtonClicked(View view) {
        setMapState(MapState.SEARCH);
    }

    public void onDirectionConfirmButtonClicked(View view) {
        try {
            googleMapServices.showDirection();
        } catch (NullPointerException e) {
            displayMessage("There must be 2 locations");
        }

    }

    private void displayMessage(String message) {
        //Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        Snackbar.make(drawerLayout, message, Snackbar.LENGTH_SHORT).show();
    }

    public void updateLocation(LatLng currentLocation) {
        comingServices.updateLocation(currentLocation);
    }

    public void onFriendFound(final ArrayList<User> friends) {
        if (friends == null)
            return;
        friendAdapter = new FriendAdapter(this, friends);
        friendAdapter.setOnItemClickListener(new FriendAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View itemView, int position) {
                setMapState(MapState.DIRECTION);
                User friend;
                if (friendPanelState == FriendPanelState.SEARCH) {
                    friend = comingServices.getFilteredFriends().get(position);
                } else {
                    friend = comingServices.getFriends().get(position);
                }
                ((EditText) (autocompleteDirectionFragment.getView()
                        .findViewById(R.id.place_autocomplete_search_input)))
                        .setText(friend.getName());
                slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
                comingServices.requestFriendLocation(friend.getId());
            }
        });
        friendAdapter.setOnItemLongClickListener(new FriendAdapter.OnItemLongClickListener() {
            @Override
            public void onItemLongClick(View itemView, int position) {
                User friend;
                if (friendPanelState == FriendPanelState.SEARCH) {
                    friend = comingServices.getFilteredFriends().get(position);
                } else {
                    friend = comingServices.getFriends().get(position);
                }
                comingServices.requestRemoveFriend(friend);
            }
        });
        RecyclerView.ItemDecoration itemDecoration = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        friendPanel.addItemDecoration(itemDecoration);
        friendPanel.setAdapter(friendAdapter);
        friendPanel.setLayoutManager(new LinearLayoutManager(this));
        slidingUpPanelLayout.setScrollableView(friendPanel);
    }

    public void onFriendLocationFound(LatLng location) {
        if (location != null) {
            googleMapServices.setDirectionMarker(location);
            googleMapServices.setLocationTo(location);
            try {
                googleMapServices.showDirection();
                googleMapServices.moveViewToLocation(googleMapServices.getFromLocation());
            } catch (NullPointerException e) {
                displayMessage("Enter your starting location");
            }
        } else {
            ((EditText) (autocompleteDirectionFragment.getView()
                    .findViewById(R.id.place_autocomplete_search_input)))
                    .setText("");
            displayMessage("Cannot retrieve your friend's location");
        }
    }

    public void onSearchFriendToggleClicked(View view) {
        if (searchFriendFragment.isHidden()
                || slidingUpPanelLayout.getPanelState() == SlidingUpPanelLayout.PanelState.COLLAPSED) {
            setFriendPanelState(FriendPanelState.SEARCH);
            slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
        } else {
            setFriendPanelState(FriendPanelState.NONE);
        }
    }

    public void onSearchFriendCancelClicked(View view) {
        setFriendPanelState(FriendPanelState.NONE);
        ((EditText)searchFriendFragment.getView().findViewById(R.id.search_friend_text)).setText("");
        friendAdapter.update(comingServices.getFriends());
    }


    public void onAddFriendToggleClicked(View view) {
        if (addFriendFragment.isHidden()
                || slidingUpPanelLayout.getPanelState() == SlidingUpPanelLayout.PanelState.COLLAPSED) {
            setFriendPanelState(FriendPanelState.ADD);
            slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
        } else {
            setFriendPanelState(FriendPanelState.NONE);
        }

    }

    private void setFriendPanelState(FriendPanelState state) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
        friendPanelState = state;
        if (friendPanelState == FriendPanelState.ADD) {
            ft.show(addFriendFragment);
            ft.hide(searchFriendFragment);
        } else if (friendPanelState == FriendPanelState.SEARCH) {
            ft.show(searchFriendFragment);
            ft.hide(addFriendFragment);
        } else if (friendPanelState == FriendPanelState.NONE) {
            ft.hide(searchFriendFragment);
            ft.hide(addFriendFragment);
        }
        ft.commit();
    }

    public void onAddFriendConfirmClicked(View view) {
        try {
            String phoneNumber = ((EditText) addFriendFragment.getView()
                    .findViewById(R.id.add_friend_number))
                    .getText()
                    .toString();
            comingServices.requestAddFriend(phoneNumber);
            hideSoftKeyboard();
        } catch (NumberFormatException e) {
            displayMessage("Invalid phone number");
        }
    }

    public void onAlreadyFriend() {
        displayMessage("Your imaginary friend's already in your list");
    }

    public void onFriendNotFound() {
        displayMessage("Your friend is imaginary");
    }

    public void onFriendNotAdded() {
        displayMessage("Cannot add friend");
    }

    public void onFriendAdded() {
        int addedPosition = comingServices.getFriends().size() - 1;
        friendAdapter.notifyItemInserted(addedPosition);
        displayMessage("Friend added");
        setFriendPanelState(FriendPanelState.NONE);
    }

    public void OnDirectionNotFound() {
        displayMessage("Cannot retrieve direction");
    }


    public void onFriendRemoved() {
        displayMessage("Friend removed");
        friendAdapter.notifyDataSetChanged();
    }

    public void onFriendNotRemoved() {
        displayMessage("Cannot remove friend");
    }

    private void hideSoftKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Log.d(TAG,"BackPressed");
    }
}
