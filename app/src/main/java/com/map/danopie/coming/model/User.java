package com.map.danopie.coming.model;

/**
 * Created by Danopie on 5/16/2017.
 */

public class User {
    private Integer id;
    private String phone;
    private String name;
    //0: female 1: male
    private Boolean gender;

    public static final Boolean GENDER_MALE = true;
    public static final Boolean GENDER_FEMALE = false;

    public User(Integer id, String name, String phone, Boolean gender){
        setId(id);
        setName(name);
        setPhone(phone);
        setGender(gender);
    }

    public Boolean getGender() {
        return gender;
    }

    public void setGender(Boolean gender) {
        this.gender = gender;
    }

    public Integer getId() {
        return id;
    }

    public String getPhone() {
        return phone;
    }

    public String getName() {
        return name;
    }


    public void setId(Integer id) {
        this.id = id;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + name.hashCode();
        result = 31 * result + id;
        result = 31 * result + phone.hashCode();
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj)
            return true;
        if(!(obj instanceof User))
            return false;
        User user = (User) obj;
        return user.id.equals(this.id);
    }
}
