package com.map.danopie.coming.service;

import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.map.danopie.coming.task.DirectionFetcher;
import com.map.danopie.coming.activity.HomeActivity;

import java.util.ArrayList;

/**
 * Created by Danopie on 5/12/2017.
 */

public class GoogleMapServices implements OnMapReadyCallback
                                        , LocationListener
                                        , GoogleApiClient.OnConnectionFailedListener
                                        , GoogleApiClient.ConnectionCallbacks
                                        , DirectionFetcher.OnDirectionFoundListener {
    private final static String TAG = GoogleMapServices.class.getSimpleName();
    private final static int DEFAULT_ZOOM = 16;
    private final static int INTERVAL = 10000;
    private final static int FASTEST_INTERVAL = 1000;
    private final static float ROUTE_WIDTH = 25;
    private static GoogleMapServices instance;

    private HomeActivity homeActivity;

    private GoogleMap map;
    private GoogleApiClient apiClient;
    private LocationRequest locationRequest;
    private LatLng currentLocation;
    private Marker searchMarker;
    private Marker directionMarker;
    private LatLng fromLocation;
    private LatLng toLocation;
    private Polyline route;

    public static GoogleMapServices getServices(HomeActivity homeActivity){
        if(instance == null) {
            instance = new GoogleMapServices(homeActivity);
        }
        return instance;
    }

    public GoogleMapServices(HomeActivity activity){
        homeActivity = activity;

        apiClient = new GoogleApiClient
                .Builder(activity)
                .addConnectionCallbacks(this)
                .addApi(LocationServices.API)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .enableAutoManage(activity,this)
                .build();
        locationRequest = new LocationRequest()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(INTERVAL)
                .setFastestInterval(FASTEST_INTERVAL);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);
    }


    public void moveViewToLocation(LatLng latLng) {
        map.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, DEFAULT_ZOOM));
    }

    public void setSearchMarker(LatLng latLng){
        if(searchMarker!=null){
            searchMarker.remove();
        }
        searchMarker = map.addMarker(new MarkerOptions()
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE))
                .position(latLng));

    }

    public void setDirectionMarker(LatLng latLng){
        if(directionMarker!=null){
            directionMarker.remove();
        }
        directionMarker = map.addMarker(new MarkerOptions()
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE))
                .position(latLng));
    }

    public LatLng getFromLocation(){
        return fromLocation;
    }

    public LatLng getToLocation(){
        return toLocation;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        Log.d(TAG,"Map ready");
        map = googleMap;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.d(TAG,"Connected to API Client");

        //Get current location for the first time since location updates haven't started yet
        Location lc = LocationServices.FusedLocationApi.getLastLocation(apiClient);

        if(lc != null){
            currentLocation = new LatLng(lc.getLatitude(),lc.getLongitude());
            Log.d(TAG,"Current location: " + currentLocation);
            homeActivity.updateLocation(currentLocation);
        }

        //Start location updates
        LocationServices.FusedLocationApi.requestLocationUpdates(apiClient,locationRequest,this);

    }


    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d(TAG,"Connection Failed");
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.d(TAG,"Connection Suspended");
    }

    @Override
    public void onLocationChanged(Location location) {
        currentLocation = new LatLng(location.getLatitude(),location.getLongitude());
        homeActivity.updateLocation(currentLocation);
    }

    public LatLng getCurrentLocation(){
        return currentLocation;
    }

    public void showDirection() throws NullPointerException {
        if(fromLocation == null || toLocation == null){
            throw new NullPointerException("Missing from or to locations");
        }
        removeRoutes();
        new DirectionFetcher()
                .setOnDirectionFoundListener(this)
                .setFromLocation(fromLocation)
                .setToLocation(toLocation)
                .execute();

    }
    public void setLocationFrom(LatLng latLng) {
        fromLocation = latLng;
    }

    public void setLocationTo(LatLng latLng){
        toLocation = latLng;
    }

    @Override
    public void onDirectionFound(ArrayList<LatLng> result) {
        if(result != null){
            setRoutes(result);
            moveViewToLocation(fromLocation);
        } else {
            homeActivity.OnDirectionNotFound();
        }

    }

    private void setRoutes(ArrayList<LatLng> points){
        PolylineOptions polylineOptions = new PolylineOptions()
                .color(Color.argb(255,78,201,226))
                .width(ROUTE_WIDTH);
        for(LatLng point: points){
            polylineOptions.add(point);
        }
        Polyline p = map.addPolyline(polylineOptions);
        route = p;
    }

    private void removeRoutes(){
        if(route != null){
            route.remove();
        }

    }

    public void clearDirection() {
        toLocation = null;
        removeRoutes();
        if(directionMarker!=null){
            directionMarker.remove();
        }
    }
}

