package com.map.danopie.coming.service;

import android.app.Activity;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.map.danopie.coming.activity.LoginActivity;
import com.map.danopie.coming.task.FriendAdder;
import com.map.danopie.coming.task.FriendListFetcher;
import com.map.danopie.coming.task.FriendRemover;
import com.map.danopie.coming.activity.HomeActivity;
import com.map.danopie.coming.task.LocationFetcher;
import com.map.danopie.coming.task.LocationUpdater;
import com.map.danopie.coming.model.User;
import com.map.danopie.coming.task.UserFetcher;

import java.util.ArrayList;

/**
 * Created by Danopie on 5/15/2017.
 */

public class ComingServices implements FriendListFetcher.OnFriendFoundListener,
        LocationFetcher.OnLocationFoundListener,
        UserFetcher.OnUserFetchedListener,
        FriendAdder.OnFriendAddedListener,
        FriendRemover.OnFriendRemovedListener{

    public static final String comingUrl = "http://coming.hol.es/";
    private static final String TAG = ComingServices.class.getSimpleName();
    private static final int TEST_USERID = 1;
    private static ComingServices instance;

    private Integer userId;
    private HomeActivity homeActivity;
    private LoginActivity loginActivity;
    private ArrayList<User> friends;
    private ArrayList<User> filteredFriends;
    private User friendToAdd;
    private User friendToRemove;
    private User currentUser;


    //Singleton implementation
    public static ComingServices getServices(Activity activity){
        if(activity instanceof HomeActivity){
            if(instance == null){
                instance = new ComingServices();
                instance.setHomeActivity((HomeActivity)activity);
                instance.connect();
            } else if(instance.homeActivity == null){
                instance.setHomeActivity((HomeActivity)activity);
                instance.connect();
            } else {
                instance.connect();
            }
        } else if(activity instanceof LoginActivity){
            if(instance == null){
                instance = new ComingServices();
                instance.setLoginActivity((LoginActivity)activity);
            } else if(instance.loginActivity == null){
                instance.setLoginActivity((LoginActivity)activity);
            }
        }
        return instance;
    }

    public void setHomeActivity(HomeActivity homeActivity){
        this.homeActivity = homeActivity;
    }

    public void setLoginActivity(LoginActivity loginActivity){
        this.loginActivity = loginActivity;
    }

    public ComingServices connect(){
        userId = currentUser.getId();
        requestAllFriends();
        requestUserInfo(userId);
        return this;
    }

    public void clearUser(){
        currentUser = null;
    }

    private void requestUserInfo(Integer userId) {
        new UserFetcher()
                .setId(userId)
                .setOnUserFetchedListener(new UserFetcher.OnUserFetchedListener() {
                    @Override
                    public void onUserFetched(User user) {
                        homeActivity.setUserInfo(user.getName(),user.getGender());
                    }
                })
                .execute();
    }


    public void requestAllFriends() {
        new FriendListFetcher()
                .setUserId(userId)
                .setOnFriendFoundListener(this)
                .execute();
    }

    public void updateLocation(LatLng location){
        new LocationUpdater()
                .setLocation(location)
                .setId(userId)
                .execute();
    }

    public ArrayList<User> getFriends(){
        return friends;
    }

    public ArrayList<User> getFilteredFriends() {
        return (filteredFriends != null)? filteredFriends :friends;
    }

    public ArrayList<User> filterFriendByName(CharSequence name){
        filteredFriends = new ArrayList<>(friends);
        for(User friend: friends){
            if(!friend.getName().contains(name)){
                filteredFriends.remove(friend);
            }
        }
        return filteredFriends;
    }

    public void requestFriendLocation(Integer friendId){
        new LocationFetcher()
                .setId(friendId)
                .setOnLocationFoundListener(this)
                .execute();
    }

    public void requestAddFriend(String phoneNumber) {
        for (User friend: friends){
            if(friend.getPhone().compareTo(phoneNumber) == 0){
                homeActivity.onAlreadyFriend();
                return;
            }
        }
        new UserFetcher()
                .setPhoneNumber(phoneNumber)
                .setOnUserFetchedListener(this)
                .execute();
    }

    public void requestRemoveFriend(User friend) {
        Integer friendId = friend.getId();

        new FriendRemover()
                .setUserId(userId)
                .setFriendId(friendId)
                .setOnFriendRemovedListener(this)
                .execute();

        friendToRemove = friend;
    }

    @Override
    public void onFriendFound(ArrayList<User> friends) {
        this.friends = friends;
        homeActivity.onFriendFound(friends);
    }

    @Override
    public void onLocationFound(LatLng location) {
        homeActivity.onFriendLocationFound(location);
    }

    @Override
    public void onUserFetched(User friend) {
        if(friend == null || userId == friend.getId()){
            homeActivity.onFriendNotFound();
            return;
        }

        new FriendAdder()
                .setUserId(userId)
                .setFriendId(friend.getId())
                .setOnFriendAddedListener(this)
                .execute();

        friendToAdd = friend;
    }

    @Override
    public void onFriendAdded(Boolean added) {
        if(added){
            friends.add(friendToAdd);
            friendToAdd = null;
            homeActivity.onFriendAdded();
        } else {
            homeActivity.onFriendNotAdded();
        }
    }

    @Override
    public void onFriendRemoved(Boolean removed) {
        if(removed){
            friends.remove(friendToRemove);
            friendToRemove = null;
            homeActivity.onFriendRemoved();
        } else {
            homeActivity.onFriendNotRemoved();
        }
    }

    public void requestLogin(String phoneNumber) {
        new UserFetcher()
                .setPhoneNumber(phoneNumber)
                .setOnUserFetchedListener(new UserFetcher.OnUserFetchedListener() {
                    @Override
                    public void onUserFetched(User user) {
                        if(user != null){
                            currentUser = user;
                            loginActivity.onUserValid();
                        } else {
                            loginActivity.onUserInvalid();
                        }
                    }
                })
                .execute();
    }
}
