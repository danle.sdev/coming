package com.map.danopie.coming.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.TextView;

import com.map.danopie.coming.R;
import com.map.danopie.coming.service.ComingServices;

/**
 * Created by Danopie on 6/14/2017.
 */

public class LoginActivity extends AppCompatActivity {
    private ComingServices comingServices;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        comingServices = ComingServices.getServices(this);

    }

    public void onLoginButtonClicked(View view) {
        Log.d("LOGIN","Button Clicked");
        TextView tv = (TextView) findViewById(R.id.login_phone_number);
        String phoneNumber = tv.getText().toString();
        if("".equals(phoneNumber)){
            displayMessage("Enter your phone number");
        } else {
            comingServices.requestLogin(phoneNumber);
        }

    }


    private void displayMessage(String message){
        ConstraintLayout constraintLayout = (ConstraintLayout) findViewById(R.id.login_main_layout);
        Snackbar.make(constraintLayout ,message,Snackbar.LENGTH_SHORT).show();
    }

    public void onUserInvalid() {
        displayMessage("Phone number or password is not valid");
    }

    public void onUserValid(){
        Intent loadIntent = new Intent(this,SplashActivity.class);
        startActivity(loadIntent);
    }
}
