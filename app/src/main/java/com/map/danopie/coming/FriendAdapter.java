package com.map.danopie.coming;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.map.danopie.coming.model.User;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Danopie on 5/16/2017.
 */

public class FriendAdapter extends RecyclerView.Adapter<FriendAdapter.ViewHolder> {
    public interface OnItemClickListener {
        void onItemClick(View itemView, int position);
    }

    public interface OnItemLongClickListener {
        void onItemLongClick(View itemView, int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView nameView;
        public ImageButton trackButton;
        public ImageView pictureView;

        public ViewHolder(final View itemView) {
            super(itemView);
            pictureView = (ImageView) itemView.findViewById(R.id.profile_picture_view);
            nameView = (TextView) itemView.findViewById(R.id.friend_name_view);
            trackButton = (ImageButton) itemView.findViewById(R.id.track_button);
            trackButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onItemClickListener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            onItemClickListener.onItemClick(itemView, position);
                        }
                    }
                }
            });
            pictureView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    if (onItemLongClickListener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            onItemLongClickListener.onItemLongClick(itemView, position);
                            lastPosition = friends.size() - 2;
                        }
                    }
                    return false;
                }
            });
        }
    }

    private OnItemLongClickListener onItemLongClickListener;
    private OnItemClickListener onItemClickListener;
    private List<User> friends;
    private Context context;
    private int lastPosition = -1;

    public void setOnItemLongClickListener(OnItemLongClickListener onItemLongClickListener) {
        this.onItemLongClickListener = onItemLongClickListener;
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public FriendAdapter(Context context, List<User> friends) {
        this.context = context;
        this.friends = friends;
    }

    private Context getContext() {
        return context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        ViewHolder viewHolder = new ViewHolder(inflater.inflate(R.layout.item_friend, parent, false));
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // Get the data model based on position
        User friend = friends.get(position);

        // Set item views based on your views and data model
        TextView textView = holder.nameView;
        textView.setText(friend.getName());

        ImageView picture = holder.pictureView;
        int pictureId = (friend.getGender())?R.drawable.profile_picture_male:R.drawable.profile_picture_female;
        picture.setBackground(getContext().getApplicationContext().getDrawable(pictureId));

        setAnimation(holder.itemView, position);
    }

    @Override
    public int getItemCount() {
        return friends.size();
    }

    private void setAnimation(View viewToAnimate, int position) {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(context, android.R.anim.fade_in);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }

    public void update(ArrayList<User> friends){
        this.friends = friends;
        notifyDataSetChanged();
    }


}
